package de.lg.player;

import java.util.Date;

/**
 * Created by Lars on 30.12.2015.
 */
public class Player {


    // spawnPosition
    public float positionX = 0.0f;
    public float positionY = 0.0f;

    private PlayerInformation playerInformation = null;

    private Date onlineTime = null;


    public Player(){
        this.onlineTime = new Date();
    }




    public String getPlayerIdentifier(){
        return playerInformation.getIdentifier();
    }

    public PlayerInformation getPlayerInformation(){
        if(playerInformation == null){
            this.playerInformation = new PlayerInformation();
        }
        return playerInformation;
    }

    public Date getOnlineTime() {
        return onlineTime;
    }

    public Player setOnlineTime(Date onlineTime) {
        this.onlineTime = onlineTime;
        return this;
    }
}
