package de.lg.player;

import de.lg.network.GlobalId;

import java.io.Serializable;

/**
 * Created by Lars on 19.01.2016.
 */
public class PlayerInformation extends GlobalId implements Serializable{

    public String name = null;

    public double score = 0;


    public double getScore() {
        return score;
    }

    public PlayerInformation setScore(double score) {
        this.score = score;
        return this;
    }

    public String getName() {
        return name;
    }

    public PlayerInformation setName(String name) {
        this.name = name;
        return this;
    }
}
