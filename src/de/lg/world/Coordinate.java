package de.lg.world;

import java.io.Serializable;

/**
 * Created by Lars on 03.01.2016.
 */
abstract class Coordinate implements Serializable {

    private int coordX = 0;
    private int coordY = 0;

    public Coordinate(int coordX, int coordY){
        this.coordX = coordX;
        this.coordY = coordY;
    }

    /**
     * getter and setter
     */

    public int getCoordX() {
        return coordX;
    }

    public Coordinate setCoordX(int coordX) {
        this.coordX = coordX;
        return this;
    }

    public int getCoordY() {
        return coordY;
    }

    public Coordinate setCoordY(int coordY) {
        this.coordY = coordY;
        return this;
    }
}
