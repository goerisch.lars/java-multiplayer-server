package de.lg.world;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.UUID;


public class ChunkHolder extends Coordinate implements Serializable {


    private Chunk[] chunks = null;

    private String transferId = "";
    /*
        logical id based on higher abstract layer
     */
    private int id = Integer.MAX_VALUE;

    /**
     * @param coordX
     * @param coordY
     * @param chunks
     */
    public ChunkHolder(int coordX, int coordY, Chunk[] chunks) {
        super(coordX, coordY);
        this.chunks = chunks;
        this.transferId = this.generateTransferId();
    }

    /**
     * @param chunk
     */
    private void registerChunkHolderToChunk(Chunk chunk) {
        // chunk.addChunkHolder(this);
    }

    /**
     * @param chunks
     */
    private void registerChunkHolderToChunks(Chunk[] chunks) {
        for (Chunk chunk : chunks) {
            registerChunkHolderToChunk(chunk);
        }
    }


    /**
     * getter and setter
     */

    public int getId() {
        return id;
    }

    public ChunkHolder setId(int id) {
        this.id = id;
        return this;
    }

    public Chunk[] getChunks() {
        return chunks;
    }

    public ChunkHolder setChunks(Chunk[] chunks) {
        this.chunks = chunks;
        return this;
    }

    public Block[] getBlocks() {

        ArrayList<Block> blocks = new ArrayList<Block>();

        for (Chunk chunk : chunks) {
            for (Block block : chunk.getBlocks()) {
                blocks.add(block);
            }

        }
        Block[] blocksArray = new Block[blocks.size()];

        for (int x = 0; x < blocks.size(); x++) {
            blocksArray[x] = blocks.get(x);
        }

        return blocksArray;
    }

    private String generateTransferId() {

        return UUID.randomUUID().toString();
    }

    public String getTransferId() {
        return this.transferId;
    }

    public void addChunk(Chunk chunk){
        for (Chunk chunk1 : chunks){
            if (chunk1 == null){
                continue;
            }
            if(chunk1.equals(chunk)){
                return;
            }
        }

        for(int x = 0; x < 9; x++){
            if( chunks[x] == null){
                chunks[x] = chunk;
                System.out.println("added chunk");
            }
        }
    }
}
