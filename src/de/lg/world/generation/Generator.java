package de.lg.world.generation;

import de.lg.world.*;
import de.lg.world.facades.Chunks;
import javafx.scene.paint.Color;

/**
 * Created by Lars on 30.12.2015.
 */
public class Generator {

    private double[][] noiseArray = null;


    public Generator(double[][] noiseArray, float coordX, float coordY, int howMuchChunks) {
        this.noiseArray = noiseArray;
    }

    /**
     * generates chunks from an noiseArray
     *
     * @param noiseArray
     * @param chunkSize
     * @return
     */
//    public Chunk[] generateChunks(double[][] noiseArray, Chunk.ChunkSize chunkSize) {
//        chunkSize = Chunk.ChunkSize.FOUR;
//        Chunk[] chunks = new Chunk[noiseArray.length * noiseArray[0].length];
//        Block[] blocks = new Block[noiseArray.length * noiseArray[0].length];
//
//        int value = 0;
//        if ((noiseArray.length % chunkSize.getValue()) == 0) {
//            for (int h = 0; h <= noiseArray.length - 1; h++) {
//                for (int j = 0; j <= noiseArray[h].length - 1; j++) {
//                    System.out.print("|" + (Double.toString(noiseArray[h][j])).substring(0, 3));
//                    blocks[value] = new Block(h, j);
//                    value++;
//                }
//                System.out.print("\n");
//            }
//            return chunks;
//        } else {
//            System.out.println("arraysize wrong Generator. generateChunks");
//        }
//        ImageWriter.greyWriteImage("generator", noiseArray);
//
//
//        return null;
//    }

    /**
     * @param chunksLength
     * @return
     */
    public ChunkHolder generateChunkHolder(float startX, float startY, int chunksLength, Chunk.ChunkSize chunkSize) {

        // noise size
        int size = chunksLength * chunkSize.getValue();

        // TODO: implement disrectoy generation in save
        // TODO: save images from chunks and chunkholders in save direcotry

        // TODO: save chunk as format to save data

        double[][] noisePattern = generateNoisePattern(size, 100, 0.5, 5000);

        // blocks
        Block[] blocks = generateBlocks(startX, startY, noisePattern);

        // chunks
        Chunk[] chunks = Chunks.generateChunksFromBlocks(blocks, chunkSize, chunksLength);
        // holder

        ChunkHolder chunkHolder = new ChunkHolder((int)startX, (int)startY, chunks);
        return chunkHolder;


    }

    public Sector generateSector(float startX, float startY, double[][] noisePattern) {

        startY = startY *-1;
        // blocks
        Block[] blocks = generateBlocks(startX, startY, noisePattern);

        return new Sector((int)startX,(int)startY,blocks);


    }

    private Block[] generateBlocks(float startX, float startY, double[][] noisePattern) {

        Block[] blocks = new Block[noiseArray.length * noiseArray[0].length];

        int stepSizeX = noiseArray.length;
        int stepSizeY = noiseArray[0].length;

        float coordXTmp = startX;
        float coordYTmp = startY;

        int arrayStepper = 0;

        Color color = new Color(Math.random() * 1, Math.random() * 1, Math.random() * 1, 1);
        // top to down
        for (double[] aNoisePattern : noisePattern) {
            // left to right
            for (double anANoisePattern : aNoisePattern) {
                blocks[arrayStepper] = new Block(coordXTmp, coordYTmp, anANoisePattern);
                coordXTmp = coordXTmp + Sectors.BLOCK_SIZE;
                arrayStepper++;
            }
            coordYTmp  = coordYTmp - Sectors.BLOCK_SIZE;
            coordXTmp = startX;
        }

        return blocks;
    }

    private double[][] generateNoisePattern(int size, int largestFeature, double persistence, int seed) {

        SimplexNoise simplexNoise = new SimplexNoise(largestFeature, persistence, seed);

        double xStart = 0;
        double XEnd = 500;
        double yStart = 0;
        double yEnd = 500;

        int xResolution = size;
        int yResolution = size;

        double[][] result = new double[xResolution][yResolution];

        for (int i = 0; i < xResolution; i++) {
            for (int j = 0; j < yResolution; j++) {
                int x = (int) (xStart + i * ((XEnd - xStart) / xResolution));
                int y = (int) (yStart + j * ((yEnd - yStart) / yResolution));
                result[i][j] = 0.5 * (1 + simplexNoise.getNoise(x, y));
            }
        }

        return result;
    }

}
