package de.lg.world.generation;

import de.lg.world.Chunk;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Lars on 03.01.2016.
 */
public class Testgenerator {

    public static void main(String args[]) {
        SimplexNoise simplexNoise = new SimplexNoise(100, 0.5, 3);


        double xStart = 0;
        double XEnd = 500;
        double yStart = 0;
        double yEnd = 500;

        int xResolution = 12;
        int yResolution = 12;

        double[][] result = new double[xResolution][yResolution];

        for (int i = 0; i < xResolution; i++) {
            for (int j = 0; j < yResolution; j++) {
                int x = (int) (xStart + i * ((XEnd - xStart) / xResolution));
                int y = (int) (yStart + j * ((yEnd - yStart) / yResolution));
                result[i][j] = 0.5 * (1 + simplexNoise.getNoise(x, y));
            }
        }

        for (int y = 0; y < result[0].length; y++) {
            for (int x = 0; x < result.length; x++) {
                System.out.print("|" + (Double.toString(result[x][y])).substring(0, 3));

            }
            System.out.print("\n");
        }

        Generator generator = new Generator(result, 0, 0, 5);

        generator.generateChunkHolder(12.0f, 12.0f, 3, Chunk.ChunkSize.FOUR);

//        for (Chunk chunk :  chunks){
//            System.out.println(chunk.getChunkId());
//        }


        Date date = new Date();
        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy#HH-mm-ss");
        String dateString = DATE_FORMAT.format(date);

        ImageWriter.greyWriteImage(dateString, result);


    }

}
