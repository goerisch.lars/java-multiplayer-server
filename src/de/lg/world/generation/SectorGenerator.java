package de.lg.world.generation;

import de.lg.world.Chunk;
import de.lg.world.ChunkHolder;
import de.lg.world.Sector;
import de.lg.world.Sectors;

import java.util.ArrayList;

/**
 * Created by Lars on 22.01.2016.
 */
public class SectorGenerator {


    public SectorGenerator() {

    }

    public ChunkHolder generateSector(float sectorX, float sectorY, boolean normalize) {

         SimplexNoise simplexNoise = new SimplexNoise(100, 0.5, generateSeed(sectorX, sectorY));


        double xStart = 0;
        double XEnd = 500;
        double yStart = 0;
        double yEnd = 500;

        int xResolution = 12;
        int yResolution = 12;

        double[][] result = new double[xResolution][yResolution];

        for (int i = 0; i < xResolution; i++) {
            for (int j = 0; j < yResolution; j++) {
                int x = (int) (xStart + i * ((XEnd - xStart) / xResolution));
                int y = (int) (yStart + j * ((yEnd - yStart) / yResolution));
                result[i][j] = 0.5 * (1 + simplexNoise.getNoise(x, y));
            }
        }

        Generator generator = new Generator(result, 0, 0, 5);

        ChunkHolder chunkHolder = generator.generateChunkHolder(sectorX, sectorY, 3, Chunk.ChunkSize.FOUR);


        int[] sectorNumber = Sectors.sectorNumber(chunkHolder.getCoordX(), chunkHolder.getCoordY());
        String pictureCoords = Sectors.generateSectorNumberString(sectorNumber[0], sectorNumber[1]);

        ImageWriter.greyWriteImage(pictureCoords, result);

        return chunkHolder;
    }

    public Sector generateSector(float sectorX, float sectorY) {



        SimplexNoise simplexNoise = new SimplexNoise(100, 0.5, generateSeed(sectorX, sectorY));

        double xStart = 0;
        double XEnd = 500;
        double yStart = 0;
        double yEnd = 500;

        int xResolution = 12;
        int yResolution = 12;

        double[][] result = new double[xResolution][yResolution];

        for (int i = 0; i < xResolution; i++) {
            for (int j = 0; j < yResolution; j++) {
                int x = (int) (xStart + i * ((XEnd - xStart) / xResolution));
                int y = (int) (yStart + j * ((yEnd - yStart) / yResolution));
                result[i][j] = 0.5 * (1 + simplexNoise.getNoise(x, y));
            }

        }


        for (int y = 0; y < result[0].length; y++) {
            for (int x = 0; x < result.length; x++) {
                if (result[x][y] > 1) {
                    result[x][y] = 1;
                }
                if (result[x][y] < 0) {
                    result[x][y] = 0;
                }
                System.out.print("|" + (Double.toString(result[x][y])).substring(0, 3));

            }
            System.out.println();
        }

        System.out.println();
        Generator generator = new Generator(result, 0, 0, 5);

        Sector sector = generator.generateSector(sectorX, sectorY, result);;

        int[] sectorNumber = Sectors.sectorNumber(sectorX, sectorY);
        String pictureCoords = Sectors.generateSectorNumberString(sectorNumber[0], sectorNumber[1]);
        //System.out.println(pictureCoords);

        ImageWriter.greyWriteImage(pictureCoords, result);

        return sector;
    }

    public static int generateSeed(float sectorX, float sectorY) {

        int worldSeed = 1457;
        int rndY = 482349;
        int rndX = -24265;

        int seed = (int) (((int) sectorX * rndX) + 14 + (int) sectorY * rndY);

        // System.out.println(seed);
        return seed;


    }


    public static void main(String args[]) {
        SectorGenerator sectorGenerator = new SectorGenerator();
        sectorGenerator.generateSector(-600, 0, true);
        // seedTest();
    }

    /**
     * test how strong the seeds from x and y values are
     */
    public static void seedTest() {
        ArrayList<Integer> seed = new ArrayList<>();

        ArrayList<Integer> doubleSeeds = new ArrayList<>();

        for (int x = -1000; x < 1000; x++) {
            System.out.print('.');
            for (int y = -1000; y < 1000; y++) {
                int seedTmp = generateSeed(x, y);
                if (seed.contains(seedTmp)) {
                    System.err.print('.');
                    doubleSeeds.add(seedTmp);
                } else {
                    seed.add(seedTmp);
                }
            }
        }
        System.err.println("double Seeds : " + doubleSeeds.size());
    }

}
