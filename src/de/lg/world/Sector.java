package de.lg.world;

import java.io.Serializable;

/**
 * new chunkHolder replacement
 *
 */
public class Sector implements Serializable{

    int x;
    int y;

    private Block[] blocks = null;

    public Sector(int x, int y, Block[] blocks){
        this.x = x;
        this.y = y;
        this.blocks = blocks;
    }

    public int getX() {
        return x;
    }

    public Sector setX(int x) {
        this.x = x;
        return this;
    }

    public int getY() {
        return y;
    }

    public Sector setY(int y) {
        this.y = y;
        return this;
    }

    public Block[] getBlocks() {
        return blocks;
    }

    public Sector setBlocks(Block[] blocks) {
        this.blocks = blocks;
        return this;
    }
}
