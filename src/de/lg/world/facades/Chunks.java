package de.lg.world.facades;

import de.lg.world.Block;
import de.lg.world.Chunk;

import java.util.ArrayList;

/**
 * Created by Lars on 03.01.2016.
 */
public class Chunks {

    public static String getChunkIdByCoords(int coordX, int coordY) {
        return coordX + "-" + coordY;
    }

    public static String getChunkIdByChunk(Chunk chunk) {
        return chunk.getChunkId();
    }

    public static Chunk[] generateChunksFromBlocks(Block[] blocks, Chunk.ChunkSize chunkSize, int chunkLength) {
        ArrayList<Chunk> chunks = new ArrayList<>();
        Block[] blocksTmp = new Block[chunkSize.getValue() * chunkSize.getValue()];
        // chunk id
        int chunkColumns = (int) Math.sqrt(blocks.length / (chunkSize.getValue() * chunkSize.getValue()));
        int chunkRows = (int) Math.sqrt(blocks.length / (chunkSize.getValue() * chunkSize.getValue()));

        int shiftValue = chunkSize.getValue() * chunkColumns;

        // cut blocksArray in row pices
        ArrayList<Block[]> blockPieces = new ArrayList<>();
        // how much elements per chunk + how much chunks
        int sliceValue = (chunkSize.getValue() * chunkSize.getValue()) * chunkColumns;

        for (int rows = 0; rows < chunkRows; rows++) {
            ArrayList<Block> blockSlicesTemp = new ArrayList<>();
            for (int stepper = sliceValue * rows; stepper < (sliceValue * rows )+ sliceValue; stepper++) {
                blockSlicesTemp.add(blocks[stepper]);
            }
            Block[] tmp = blockSlicesTemp.toArray(new Block[sliceValue]);
            blockPieces.add(tmp);
        }

        // each array entry contains 1 row of chunks now we can switch easy with shifting

        for (Block[] blockPiece : blockPieces) {

            for (int chunkNumber = 0; chunkNumber < chunkColumns; chunkNumber++) {
                ArrayList<Block> chunkBlocks = new ArrayList<>();
                for (int rows = 0; rows < chunkSize.getValue(); rows++) {
                    int rowShift = rows * shiftValue;
                    for (int sizeStepper = 0; sizeStepper < chunkSize.getValue(); sizeStepper++) {
                        int sizeChunkStepper = sizeStepper + chunkNumber;
                        int blockStepper = sizeChunkStepper + rowShift + (chunkNumber * (chunkSize.getValue() - 1));
                        chunkBlocks.add(blockPiece[blockStepper]);
                    }
                }
                // chunk ready
                Block[] blockArray = chunkBlocks.toArray(new Block[sliceValue]);
                Chunk chunk = new Chunk(blockArray, (int) blockArray[0].getCoordX(), (int) blockArray[0].getCoordX());
                chunks.add(chunk);
            }
        }

        Chunk[] chunksArray = chunks.toArray(new Chunk[blocks.length / (chunkSize.getValue() * chunkSize.getValue())]);


        return chunksArray;
    }
}


