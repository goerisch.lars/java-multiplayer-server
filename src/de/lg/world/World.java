package de.lg.world;

import de.lg.network.messages.KiMessage;
import de.lg.network.messages.MovementMessage;
import de.lg.network.messages.WorldSectorMessage;
import de.lg.player.Player;
import de.lg.simulation.SimulationMessageHandler;
import de.lg.world.generation.SectorGenerator;

import java.awt.*;
import java.util.*;

/**
 * Created by Lars on 19.01.2016.
 */
public class World {

    public HashMap<String, Player> players = new HashMap<>();

    public HashMap<String, Player> ki = new HashMap<>();

    public final HashMap<String, Sector> sectors = new HashMap<>();

    public final ArrayList<Item> items = new ArrayList<>();

    private HashMap<String, ChunkHolder> sectorMap = new HashMap<>();

    public ArrayList<Block> obstracles = new ArrayList<>();

    public Player getPlayerByIdentifier(String identifier) {
        return this.players.get(identifier);
    }

    private void addPlayer(Player player) {
        this.players.put(player.getPlayerIdentifier(), player);
    }

    private HashMap<Player, Integer> scoreMap = new HashMap<>();

    private SimulationMessageHandler simulationMessageHandler;

    private float spawnDistance = 500;

    public World() {
        updateWithIntervall();
    }

    public SimulationMessageHandler getSimulationMessageHandler() {
        return simulationMessageHandler;
    }

    public World setSimulationMessageHandler(SimulationMessageHandler simulationMessageHandler) {
        this.simulationMessageHandler = simulationMessageHandler;
        return this;
    }

    public boolean playerIsOnline(String identifier) {
        Player player1 = getPlayerByIdentifier(identifier);
        if (player1 != null) {
            return true;
        }
        return false;
    }


    public Player loadPlayer(Player player) {
        this.addPlayer(player);

        return player;
    }

    /**
     * spanw an player on world zone start position
     *
     * @param player
     */
    public void spawnPlayer(Player player) {
        player.positionX = 0.0f;
        player.positionY = 0.0f;


        addPlayer(player);
    }

    public Sector getSector(int x, int y) {
        String id = Sectors.generateSectorNumberString(x, y);
        return this.sectors.get(id);
    }

    /**
     * checks if the sector around the player are loaded or if need to hold more
     *
     * @param player
     * @param loadSector - define if sector loaded from the server if not loaded
     */
    private boolean validateWorldSectors(Player player, boolean loadSector) {
        float[][] sectorCoords = Sectors.getSectorCoordsAround(player.positionX, player.positionY);

        for (float[] sectorCoord : sectorCoords) {
            boolean isLoaded = this.sectorIsLoaded(sectorCoord[0], sectorCoord[1]);
            if (!isLoaded) {
                if (loadSector) {
                    this.generateSector(sectorCoord[0], sectorCoord[1]);
                }
            }
        }
        return true;
    }


    public Sector generateSector(float x, float y) {
        // TODO: Sector is saved

        SectorGenerator sectorGenerator = new SectorGenerator();
//        ChunkHolder chunkHolder = sectorGenerator.generateSector(x, y, false);
        String id = Sectors.generateSectorNumberString(x, y);

        Sector sector = sectorGenerator.generateSector(x, y);
        this.sectors.put(id, sector);

        Block[] block = sector.getBlocks();
        for (Block block1 : block) {
            if (block1 != null && block1.value > 0.7f) {
                this.obstracles.add(block1);
            }
        }
        return sector;
    }

    public boolean sectorIsLoaded(float x, float y) {
        Sector sector = this.sectors.get(Sectors.generateSectorNumberString(x, y));
        if (sector == null) {
            return false;
        } else {
            return true;
        }
    }

    public void checkItems(Player player, KiMessage kiMessage) {
        Item tmp = null;
        for (Item item : items) {
            Rectangle r = new Rectangle((int) item.getX(), (int) item.getY(), 25, 25);
            Rectangle p = new Rectangle((int) player.positionX, (int) player.positionY, 50, 50);


            if (r.getBounds().intersects(p)) {
                System.err.println(r.getX() + " " + r.getY() + "|" + p.getX() + " " + p.getY());
                WorldSectorMessage worldSectorMessage1 = new WorldSectorMessage();
                worldSectorMessage1.setAuthenticationToken("SERVERMESSAGE");
                worldSectorMessage1.type = "DESTROY";
                worldSectorMessage1.setData(item.toString());
                worldSectorMessage1.setData(item.toString());
                this.getSimulationMessageHandler().getSimulation().addMessageToNetwork(worldSectorMessage1);

                WorldSectorMessage worldSectorMessage2 = new WorldSectorMessage();
                worldSectorMessage2.setAuthenticationToken(kiMessage.getAuthenticationToken());
                worldSectorMessage2.type = "SCORE";
                worldSectorMessage2.setData(item.toString());
                this.getSimulationMessageHandler().getSimulation().addMessageToNetwork(worldSectorMessage2);

                tmp = item;
                Integer score = scoreMap.get(player);
                if (score != null) {
                    scoreMap.put(player, score + 1);
                } else {
                    scoreMap.put(player, 1);
                }


                System.err.println("score");
                break;
            }
        }

        if (tmp != null) {
            items.remove(tmp);
        }

    }


    public boolean CollisionTest(Block one, Block two) {

        Rectangle r = new Rectangle((int) two.getCoordX(), (int) two.getCoordY(), 50, 50);
        Rectangle p = new Rectangle((int) one.getCoordX(), (int) one.getCoordY(), 50, 50);


        if (r.getBounds().intersects(p)) {
            System.err.println(r.getX() + " " + r.getY() + "|" + p.getX() + " " + p.getY());
            return true;
        }
        return false;
    }

    public void updatePlayerPosition(MovementMessage movementMessage) {

        float x = movementMessage.getMoveToX();
        float y = movementMessage.getMoveToY();

        Player player = getPlayerByIdentifier(movementMessage.getAuthenticationToken());

        boolean hasCollision = checkForCollision(player, movementMessage);
        if (!hasCollision) {
            player.positionX = x;
            player.positionY = y;
        }


    }

    private boolean checkForCollision(Player player, MovementMessage movementMessage) {
        Block playerBlock = new Block(player.positionX, player.positionY, 1);

        if (player.positionX > 500 || player.positionX < -530) {
            movementMessage.setMoveIsValid(false);
            movementMessage.setMoveToX(movementMessage.moveToX - 5);
            movementMessage.setMoveToY(movementMessage.moveToY);
            player.positionX = movementMessage.moveToX - 5;
            player.positionY = movementMessage.moveToY;
            return true;
        }
        for (Block block : obstracles) {
            // boolean hasCollision = CollisionTest(playerBlock, block);
            if (!true) {
                movementMessage.setMoveIsValid(false);
                movementMessage.setMoveToX(0);
                movementMessage.setMoveToY(0);
                player.positionX = 0;
                player.positionY = 0;
                return true;
            } else {
                movementMessage.setMoveIsValid(true);
            }
        }
        movementMessage.setMoveIsValid(true);
        return false;
    }

    public void updateWorld() {


        for (HashMap.Entry<String, Player> entry : players.entrySet()) {
            if (this.sectors.size() > 12 * players.size()) {
                this.sectors.clear();
                System.err.println("clear memory");
            }


            //todo Performance
            float[][] sectorCoords = Sectors.getSectorCoordsAround(entry.getValue().positionX, entry.getValue().positionY);
            for (float[] sectorCoord : sectorCoords) {
                boolean isLoaded = this.sectorIsLoaded(sectorCoord[0], sectorCoord[1]);
                if (!isLoaded) {
                    this.generateSector(sectorCoord[0], sectorCoord[1]);
                }
            }


        }

    }

    public void spawnItems() {


        if (!(items.size() > 10 * players.size())) {
            int minValueX = -500;
            int maxValueX = 500;

            int minValueY = (int) spawnDistance;
            int maxValueY = minValueY + (2000 * players.size());

            int newItems = (int)(10 * players.size() - items.size());


            Random rand = new Random();

            String data = "";
            for (int x = 0; x < newItems; x++) {
                int randomNumX = rand.nextInt((maxValueX - minValueX) + 1) + minValueX;
                int randomNumY = rand.nextInt((maxValueY - minValueY) + 1) + minValueY;
                Item item = new Item();
                item.setX(randomNumX);
                item.setY(randomNumY * -1);
                this.items.add(item);
                data = data + item.toString();
            }
            System.err.println(items.size());
            WorldSectorMessage worldSectorMessage = new WorldSectorMessage();
            worldSectorMessage.setAuthenticationToken("SERVERMESSAGE");
            worldSectorMessage.type = "ITEM";
            worldSectorMessage.setData(data);
            getSimulationMessageHandler().getSimulation().addMessageToNetwork(worldSectorMessage);


        } else {

            for (Item item : items) {
                WorldSectorMessage worldSectorMessage = new WorldSectorMessage();
                worldSectorMessage.setAuthenticationToken("SERVERMESSAGE");
                worldSectorMessage.type = "ITEM";
                worldSectorMessage.setData(item.toString());
                getSimulationMessageHandler().getSimulation().addMessageToNetwork(worldSectorMessage);
            }

            System.err.println("data");
        }
    }

    public int[] largestPlayerDistance() {

        int[] coord = new int[2];

        for (Player player : players.values()) {
            if (player.positionY > coord[1]) {
                coord[0] = (int) player.positionX;
                coord[1] = (int) player.positionY;

            }
        }

        coord[1] = coord[1] + 2000;

        return coord;
    }

    public void updateWithIntervall() {
        Timer timer = new Timer();

        // Start in 2 Sekunden
        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                spawnItems();

            }
        }, 5 * 1000, 10000);


    }


}
