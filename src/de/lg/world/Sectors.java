package de.lg.world;

/**
 * Created by Lars on 22.01.2016.
 */
public abstract class Sectors {

    public static float BLOCK_SIZE = 50;

    // sector size in BLOCKS
    public static int SECTOR_SIZE = 12;

    public static String generateSectorNumberString(int x, int y) {
        return x + "-" + y;
    }

    public static String generateSectorNumberString(float x, float y) {
        return (int) x + "-" + (int) y;
    }

    public static float[][] getSectorCoordsAround(float x, float y) {

        float[] sectorCoord = Sectors.sectorCoords(x, y);

        x = sectorCoord[0];
        y = sectorCoord[1];

        float[][] sectorCoords = new float[9][2];

        // sectormap around the player
        // 0 1 2
        // 3 4 5
        // 6 7 8

        // 0
        sectorCoords[0] = new float[]{x - (BLOCK_SIZE * SECTOR_SIZE), y + ((BLOCK_SIZE * SECTOR_SIZE))};
        // 1
        sectorCoords[1] = new float[]{x, y + ((BLOCK_SIZE * SECTOR_SIZE))};
        // 2
        sectorCoords[2] = new float[]{x + (BLOCK_SIZE * SECTOR_SIZE), y + ((BLOCK_SIZE * SECTOR_SIZE))};
        // 3
        sectorCoords[3] = new float[]{x - (BLOCK_SIZE * SECTOR_SIZE), y};
        // 4
        sectorCoords[4] = new float[]{x, y};
        // 5
        sectorCoords[5] = new float[]{x + (BLOCK_SIZE * SECTOR_SIZE), y};
        // 6
        sectorCoords[6] = new float[]{x - ((BLOCK_SIZE * SECTOR_SIZE)), y - ((BLOCK_SIZE * SECTOR_SIZE))};
        // 7
        sectorCoords[7] = new float[]{x, y - ((BLOCK_SIZE * SECTOR_SIZE))};
        // 8
        sectorCoords[8] = new float[]{x + (BLOCK_SIZE * SECTOR_SIZE), y - ((BLOCK_SIZE * SECTOR_SIZE))};

        return sectorCoords;
    }

    /**
     * sectorNumber by given coords
     * <p>
     * +x = x | -x = x + 1
     * +y = y + 1  | -y = y
     *
     * @param x
     * @param y
     * @return
     */
    public static int[] sectorNumber(float x, float y) {

        int sectorX = 0;
        int sectorY = 0;

        if (x >= 0) {
            sectorX = ((int) (x / (BLOCK_SIZE * SECTOR_SIZE))) + 1;
        } else {
            if (x % (BLOCK_SIZE * SECTOR_SIZE) == 0) {
                x = x + 1;
            }
            sectorX = ((int) (x / (BLOCK_SIZE * SECTOR_SIZE)));
        }

        if (y > 0) {
            if (y % (BLOCK_SIZE * SECTOR_SIZE) == 0) {
                y = y - 1;
            }
            sectorY = ((int) (y / (BLOCK_SIZE * SECTOR_SIZE))) + 1;
        } else {
            sectorY = ((int) (y / (BLOCK_SIZE * SECTOR_SIZE)));
        }

        return new int[]{(int) sectorX, (int) sectorY};
    }

    /**
     * @param x
     * @param y
     * @return
     */
    public static float[] sectorCoords(float x, float y) {
        int[] result = Sectors.sectorNumber(x, y);

        int sectorX = result[0];
        int sectorY = result[1];

        if (sectorX >= 0) {
            sectorX = sectorX - 1;
        }

        return new float[]{sectorX * (BLOCK_SIZE * SECTOR_SIZE), sectorY * (BLOCK_SIZE * SECTOR_SIZE)};
    }

    /**
     * @param x
     * @param y
     * @return
     */
    public static float[] sectorCoordsByNumber(int x, int y) {


        float sectorX = x >= 0 ? (x - 1) * (BLOCK_SIZE * SECTOR_SIZE) : (x - 1) * (BLOCK_SIZE * SECTOR_SIZE);
        float sectorY = y >= 0 ? (y) * (BLOCK_SIZE * SECTOR_SIZE) : (y) * (BLOCK_SIZE * SECTOR_SIZE);

        return new float[]{sectorX, sectorY};
    }


    public static void testSectorCoords() {
// TODO: fix
//        // x , y
//        System.out.println("###### sectorNumber ######");
//        int[] aTesT = Sectors.sectorNumber(-600, 0);
//        System.out.println(aTesT[0] + " " + aTesT[1] + " erwartet: " + 0 + " " + 0);
//
//        int[] bTesT = Sectors.sectorNumber(0, -600);
//        System.out.println(bTesT[0] + " " + bTesT[1] + " erwartet: " + 1 + " " + 0);
//
//        int[] cTesT = Sectors.sectorNumber(-2400, -900);
//        System.out.println(cTesT[0] + " " + cTesT[1] + " erwartet: " + -3 + " " + -1);
//
//        int[] dTesT = Sectors.sectorNumber(1500, 2100);
//        System.out.println(dTesT[0] + " " + dTesT[1] + " erwartet: " + 2 + " " + 4);
//
//        System.out.println("###### sectorCoords ######");
//        float[] eTesT = Sectors.sectorCoords(0, 5);
//        System.out.println(eTesT[0] + " " + eTesT[1] + " erwartet: " + 0 + " " + 600);
//
//        float[] fTesT = Sectors.sectorCoords(600, -300);
//        System.out.println(fTesT[0] + " " + fTesT[1] + " erwartet: " + 600 + " " + 0);
//
//        float[] gTesT = Sectors.sectorCoords(-2100, -900);
//        System.out.println(gTesT[0] + " " + gTesT[1] + " erwartet: " + -2400 + " " + -600);
//
//        float[] hTesT = Sectors.sectorCoords(1200, 2100);
//        System.out.println(hTesT[0] + " " + hTesT[1] + " erwartet: " + 1200 + " " + 2400);
//
//        System.out.println("###### sectorCoordsByNumber ######");
//        float[] iTesT = Sectors.sectorCoordsByNumber(-0, -1);
//        System.out.println(iTesT[0] + " " + iTesT[1] + " erwartet: " + 0 + " " + 600);
//
//        float[] jTesT = Sectors.sectorCoordsByNumber(-1, 0);
//        System.out.println(jTesT[0] + " " + jTesT[1] + " erwartet: " + -600 + " " + 0);
//
//        float[] kTesT = Sectors.sectorCoordsByNumber(-4, -1);
//        System.out.println(kTesT[0] + " " + kTesT[1] + " erwartet: " + -2400 + " " + -600);
//
//        float[] lTesT = Sectors.sectorCoordsByNumber(2, -4);
//        System.out.println(lTesT[0] + " " + lTesT[1] + " erwartet: " + 1200 + " " + -2400);
    }


    /**
     * tests for function
     *
     * @param args
     */
    public static void main(String[] args) {
        testSectorCoords();
        System.out.println("###### sectorCoords ######");
        float[] eTesT = Sectors.sectorCoords(650, 601);
        System.out.println(eTesT[0] + " " + eTesT[1] + " erwartet: " + 0 + " " + 600);

    }
}
