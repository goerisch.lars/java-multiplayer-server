package de.lg.world;

import de.lg.world.facades.Chunks;

import java.io.Serializable;

/**
 * Created by Lars on 30.12.2015.
 * <p>
 * a chunk holds n Block's for representing a map section
 */
public class Chunk extends Coordinate implements Serializable {

    private String chunkHolderTransferId = null;

    public enum ChunkSize {
        ONE(1),
        FOUR(4),
        NINE(9),
        SIXTEEN(16);

        private int value;

        private ChunkSize(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    private Block[] blocks = null;

    public Chunk(Block[] blocks, int coordX, int coordY) {
        super(coordX, coordY);
        this.blocks = blocks;
    }

    public String getChunkId() {
        return Chunks.getChunkIdByCoords(this.getCoordX(), this.getCoordY());
    }


    /**
     * getter and setter
     */
    public Block[] getBlocks() {
        return blocks;
    }

    public void setBlocks(Block[] blocks) {
        this.blocks = blocks;
    }

    public void setTransferId(String id) {
        this.chunkHolderTransferId = id;
    }
}
