package de.lg.world;



import java.io.Serializable;

/**
 * Created by Lars on 30.12.2015.
 *
 * a block represents an square on the map with 1 m x 1m length
 * the block is starting at the left top point
 */
public class Block extends CoordinateFloat implements Serializable {

    String texturePath = null;

    boolean blocked = false;



    double value;

    /**
     *
     * @param coordX
     * @param coordY
     * @param value double value from noisePattern
     */
    public Block(float coordX, float coordY, double value){
        super(coordX,coordY);
        this.value = value;
    }

    @Override
    public String toString() {
        return this.getCoordX() + "#"  + this.getCoordY() + "a";
    }
}
