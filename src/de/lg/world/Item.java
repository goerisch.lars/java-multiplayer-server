package de.lg.world;

/**
 * Created by Lars on 23.01.2016.
 */
public class Item {

    int x = 0;
    int y = 0;

    public Item(){

    }

    public int getY() {
        return y;
    }

    public Item setY(int y) {
        this.y = y;
        return this;
    }

    public int getX() {
        return x;
    }

    public Item setX(int x) {
        this.x = x;
        return this;
    }

    public String toString() {
        return this.x + "#"  + this.y + "a";
    }
}
