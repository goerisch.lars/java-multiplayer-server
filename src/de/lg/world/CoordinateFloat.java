package de.lg.world;

import java.io.Serializable;

/**
 * Created by Lars on 03.01.2016.
 */
abstract class CoordinateFloat implements Serializable {

    private float coordX = 0;
    private float coordY = 0;

    public CoordinateFloat(float coordX, float coordY){
        this.coordX = coordX;
        this.coordY = coordY;
    }


    /**
     * getter and setter
     */

    public float getCoordX() {
        return coordX;
    }

    public CoordinateFloat setCoordX(float coordX) {
        this.coordX = coordX;
        return this;
    }

    public float getCoordY() {
        return coordY;
    }

    public CoordinateFloat setCoordY(float coordY) {
        this.coordY = coordY;
        return this;
    }
}
