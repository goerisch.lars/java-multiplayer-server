package de.lg.simulation;

import de.lg.network.Message;
import de.lg.network.messages.WorldSectorMessage;
import de.lg.world.*;

/**
 * Created by Lars on 22.01.2016.
 */
public class SimulationWorldSectorMessageHandler implements SimulationMessageHandlerI {
    private SimulationMessageHandler networkMessageHandler = null;

    public SimulationWorldSectorMessageHandler(SimulationMessageHandler networkMessageHandler) {
        this.networkMessageHandler = networkMessageHandler;
    }

    @Override
    public void handleMessage(Message message) {
        WorldSectorMessage worldSectorMessage = (WorldSectorMessage) message;
        World world = networkMessageHandler.getSimulation().getWorld();
        if(worldSectorMessage.type.equals("POINTS")){
            //world.checkItems(world.getPlayerByIdentifier(worldSectorMessage.getAuthenticationToken()), worldSectorMessage);


        }else{
            String data = "";
            int stepper = 0;
            for(Block block : world.obstracles){
                if(block != null){
                    data = data + block.toString();
                    stepper = stepper +1;
                    if (stepper % 10 == 0){
                        stepper = 0;
                        WorldSectorMessage worldSectorMessage1 = new WorldSectorMessage();
                        worldSectorMessage1.setAuthenticationToken(message.getAuthenticationToken());
                        worldSectorMessage1.setData(new String(data.toString()));
                        this.networkMessageHandler.getSimulation().addMessageToNetwork(worldSectorMessage1);
                        data = "";

                    }
                }
            }
            worldSectorMessage.setData(data);
        }


        this.networkMessageHandler.getSimulation().addMessageToNetwork(worldSectorMessage);

    }



    @Override
    public void announce() {

    }

    @Override
    public Class getHandledClass() {
        return WorldSectorMessage.class;
    }
}
