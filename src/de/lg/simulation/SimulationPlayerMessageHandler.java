package de.lg.simulation;

import de.lg.network.Message;
import de.lg.network.messages.PlayerMessage;
import de.lg.player.Player;

import java.util.HashMap;

/**
 * Created by Lars on 19.01.2016.
 */
public class SimulationPlayerMessageHandler implements SimulationMessageHandlerI {

    private SimulationMessageHandler simulationMessageHandler = null;


    public SimulationPlayerMessageHandler(SimulationMessageHandler simulationMessageHandler) {
        this.simulationMessageHandler = simulationMessageHandler;
    }

    @Override
    public Class getHandledClass() {
        return PlayerMessage.class;
    }

    @Override
    public void announce() {

    }

    @Override
    public void handleMessage(Message message) {
        // check if player is online if not create on and add them to world
        PlayerMessage playerMessage = (PlayerMessage) message;

        if (!this.simulationMessageHandler.getSimulation().getWorld().playerIsOnline(playerMessage.getPlayerIdentifier())) {
            Player player = new Player();
            player.getPlayerInformation().setIdentifier(playerMessage.getAuthenticationToken());
            player.getPlayerInformation().setAuthenticationToken(playerMessage.getAuthenticationToken());
            player.getPlayerInformation().setName("heinrich test");
            Player player1 = this.simulationMessageHandler.getSimulation().getWorld().loadPlayer(player);

            playerMessage.setPositionX(player1.positionX);
            playerMessage.setPositionY(player1.positionY);

            playerMessage.playerInformation = player1.getPlayerInformation();

        } else {
            System.out.println("player already online disconnect");
            // TODO: disconnect player
        }

        for (HashMap.Entry<String, Player> entry : this.simulationMessageHandler.getSimulation().getWorld().players.entrySet()) {
            PlayerMessage playerMessage1 = new PlayerMessage();

            playerMessage1.setPlayerIdentifier(message.getAuthenticationToken());
            playerMessage1.setPositionX(0);
            playerMessage1.setPositionY(0);
            playerMessage1.setAuthenticationToken(entry.getKey());

            this.simulationMessageHandler.getSimulation().addMessageToNetwork(playerMessage1);
        }

    }
    // send information to network stack


}
