package de.lg.simulation;

import de.lg.core.Simulation;
import de.lg.core.SynchronizedQueue;
import de.lg.network.Message;

import java.util.HashMap;

/**
 * Created by Lars on 19.01.2016.
 */
public class SimulationMessageHandler {

    public SynchronizedQueue<Message> simulationNetworkQueue = null;
    public HashMap<String, SimulationMessageHandlerI> messageHandlers = new HashMap<>();

    private Simulation simulation = null;

    public SimulationMessageHandler(Simulation simulation) {
        this.simulation = simulation;
        initMessageHandlers();
    }

    public void initMessageHandlers() {

        SimulationPlayerMessageHandler simulationPlayerMessageHandler = new SimulationPlayerMessageHandler(this);
        registerHandler(simulationPlayerMessageHandler);

        SimulationWorldSectorMessageHandler worldSectorMessageHandler = new SimulationWorldSectorMessageHandler(this);
        registerHandler(worldSectorMessageHandler);

        SimulationMovementMessageHandler simulationMovementMessageHandler = new SimulationMovementMessageHandler(this);
        registerHandler(simulationMovementMessageHandler);

        SimulationKiMessageHandler simulationKiMessageHandler = new SimulationKiMessageHandler(this);
        registerHandler(simulationKiMessageHandler);
    }

    /**
     * register an handler
     *
     * @param handler
     */
    private void registerHandler(SimulationMessageHandlerI handler) {
        messageHandlers.put(handler.getClass().toString(), handler);
    }


    public void proceedNetworkMessage(Object object) {

        for (HashMap.Entry<String, SimulationMessageHandlerI> entry : messageHandlers.entrySet()) {
            Class handledClass = entry.getValue().getHandledClass();

            if (object.getClass().equals(handledClass)) {
               entry.getValue().handleMessage((Message) object);
            }
        }
        try {
            // throw new Exception("NetworkMessageHandler object of type NetworkMessage expected got " + object.getClass());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public Simulation getSimulation(){
        return this.simulation;
    }
}
