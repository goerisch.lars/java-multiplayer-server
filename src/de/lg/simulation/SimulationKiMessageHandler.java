package de.lg.simulation;

import de.lg.network.Message;
import de.lg.network.messages.KiMessage;
import de.lg.player.Player;
import de.lg.world.World;

/**
 * Created by Lars on 23.01.2016.
 */
public class SimulationKiMessageHandler implements SimulationMessageHandlerI {

    private SimulationMessageHandler simulationMessageHandler = null;

    public SimulationKiMessageHandler(SimulationMessageHandler simulationMessageHandler) {
        this.simulationMessageHandler = simulationMessageHandler;

    }

    @Override
    public void handleMessage(Message message) {
        KiMessage message1 = (KiMessage) message;
        World world = this.simulationMessageHandler.getSimulation().getWorld();
        Player ki = world.ki.get(message1.getKiId());
        if(ki != null){
            ki.positionX = message1.getMoveToX();
            ki.positionY = message1.getMoveToY();
            world.checkItems(ki,message1);
        }else{
            ki = new Player();
            ki.positionX = message1.getMoveToX();
            ki.positionY = message1.getMoveToY();
            ki.getPlayerInformation().setAuthenticationToken(message1.getKiId());
            world.ki.put(message1.getKiId(),ki);
            world.checkItems(ki,message1);
        }
        this.simulationMessageHandler.getSimulation().addMessageToNetwork(message);
    }

    @Override
    public void announce() {

    }

    @Override
    public Class getHandledClass() {
        return KiMessage.class;
    }
}
