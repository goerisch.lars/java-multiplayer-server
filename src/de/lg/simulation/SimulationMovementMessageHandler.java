package de.lg.simulation;

import de.lg.network.Message;
import de.lg.network.messages.MovementMessage;
import de.lg.player.Player;

import java.util.HashMap;

/**
 * Created by Lars on 23.01.2016.
 */
public class SimulationMovementMessageHandler implements SimulationMessageHandlerI {


    private SimulationMessageHandler simulationMessageHandler = null;

    public SimulationMovementMessageHandler(SimulationMessageHandler networkMessageHandler) {
        this.simulationMessageHandler = networkMessageHandler;
    }


    @Override
    public void handleMessage(Message message) {
        MovementMessage movementMessage = (MovementMessage) message;
        if(!movementMessage.isKI()){
            this.simulationMessageHandler.getSimulation().getWorld().updatePlayerPosition(movementMessage);
        }


        for (HashMap.Entry<String, Player> entry : this.simulationMessageHandler.getSimulation().getWorld().players.entrySet()) {
            MovementMessage movementMessage1 = new MovementMessage();
            if(movementMessage.isKI()){
                System.err.println("KI");
                movementMessage1.setPlayerId(movementMessage.getKiId());
            }else{
                movementMessage1.setPlayerId(message.getAuthenticationToken());
            }
            movementMessage1.setPlayerId(message.getAuthenticationToken());
            movementMessage1.setMoveIsValid(movementMessage.isMoveIsValid());
            movementMessage1.setMoveToX(movementMessage.moveToX);
            movementMessage1.setMoveToY(movementMessage.moveToY);
            movementMessage1.setAuthenticationToken(entry.getKey());
            movementMessage1.setKI(movementMessage.isKI());

            this.simulationMessageHandler.getSimulation().addMessageToNetwork(movementMessage1);
        }
        //this.simulationMessageHandler.getSimulation().addMessageToNetwork(message);
    }

    @Override
    public void announce() {

    }

    @Override
    public Class getHandledClass() {
        return MovementMessage.class;
    }
}
