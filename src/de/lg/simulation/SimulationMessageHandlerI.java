package de.lg.simulation;

import de.lg.network.Message;

/**
 * Created by Lars on 19.01.2016.
 */
public interface SimulationMessageHandlerI {

        public void handleMessage(Message message);

        public void announce();

        public Class getHandledClass();
}
