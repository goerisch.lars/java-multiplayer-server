package de.lg.core;

/**
 * Created by Lars on 02.01.2016.
 */
public class Config {

    public final static int serverPort = 9999;
    public final static int worldSeed = 5000;

}
