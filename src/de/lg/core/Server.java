package de.lg.core;

import de.lg.network.Message;
import de.lg.network.NetworkListener;

/**
 * Created by Lars on 30.12.2015.
 */
public class Server implements Runnable {

    public boolean serverIsRunning = true;

    private Thread simulationThread = null;
    private Thread networkThread = null;

    NetworkListener networkListener = null;

    public static void main(String [] args)
    {
        Server server = new Server();
        server.run();
    }

    @Override
    public void run() {
        //preparation
        SynchronizedQueue<Message> networkSimulationQueue = new SynchronizedQueue<>();
        SynchronizedQueue<Message> simulationNetworkQueue = new SynchronizedQueue<>();

        //start simulation
        this.simulationThread = new Thread( new Simulation(networkSimulationQueue,simulationNetworkQueue ));
        this.simulationThread.start();

        // start network
        this.networkThread = new Thread( new NetworkListener(networkSimulationQueue,simulationNetworkQueue));
        this.networkThread.start();

        System.out.println("Server running");
        while (serverIsRunning){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean getServerState(){
        return this.serverIsRunning;
    }
}