package de.lg.core;

import de.lg.network.Message;
import de.lg.simulation.SimulationMessageHandler;
import de.lg.world.World;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Lars on 02.01.2016.
 */
public class Simulation implements Runnable {

    // network -> simulation
    private SynchronizedQueue<Message> networkSimulationQueue;

    // simulation -> network
    private SynchronizedQueue<Message> simulationNetworkQueue;

    private Collection<Message> messageStack = new ArrayList<>();

    private SimulationMessageHandler simulationMessageHandler = null;

    private World world = null;

    public Simulation(SynchronizedQueue<Message> networkSimulationQueue, SynchronizedQueue<Message> simulationNetworkQueue) {
        this.networkSimulationQueue = networkSimulationQueue;
        this.simulationNetworkQueue = simulationNetworkQueue;
    }

    @Override
    public void run() {
        // init
        this.simulationMessageHandler = new SimulationMessageHandler(this);

        this.world = new World();
        world.setSimulationMessageHandler(simulationMessageHandler);

        System.out.println("Simulation running");

        while (true) {
            proceedQueryMessages();
            updatePlayerPositions();
            updateKIPositions();
            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


    }

    /**
     * sort all messages in types
     */
    private void proceedQueryMessages() {
        if (!this.networkSimulationQueue.isEmpty()) {
            messageStack = this.networkSimulationQueue.getAll(true);
            for (Message message : messageStack) {
                this.simulationMessageHandler.proceedNetworkMessage(message);
            }
        }
    }

    private void updateKIPositions() {


    }

    private void updatePlayerPositions() {

        this.world.updateWorld();

    }


    public World getWorld(){
        return this.world;
    }

    public void addMessageToNetwork(Message message){
        this.simulationNetworkQueue.add(message);
    }
}
