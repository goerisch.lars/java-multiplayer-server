package de.lg.network;

import java.nio.channels.AsynchronousSocketChannel;

/**
 * Created by Lars on 30.12.2015.
 *
 * holds an connection between the server and an specific client
 */
public class ClientConnection extends Connection{

    public ClientConnection(AsynchronousSocketChannel asynchronousSocketChannel) {
        super(asynchronousSocketChannel);
    }
}
