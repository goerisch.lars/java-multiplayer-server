package de.lg.network.handler;

import de.lg.network.Connection;
import de.lg.network.Message;
import de.lg.network.NetworkMessageHandler;
import de.lg.network.messages.WorldSectorMessage;

/**
 * Created by Lars on 18.01.2016.
 */
public class WorldSectorMessageHandler implements MessageHandler {

    private NetworkMessageHandler networkMessageHandler = null;

    public WorldSectorMessageHandler(NetworkMessageHandler networkMessageHandler){
        this.networkMessageHandler = networkMessageHandler;
    }

    @Override
    public void handleMessage(Connection clientConnection, Message message) {

        this.networkMessageHandler.getNetworkListener().addMessageToSimulation(message);
    }

    @Override
    public void announce() {

    }

    @Override
    public Class getHandledClass() {
        return WorldSectorMessage.class;
    }
}