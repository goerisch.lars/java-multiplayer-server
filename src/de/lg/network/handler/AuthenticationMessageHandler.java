package de.lg.network.handler;

import de.lg.network.Connection;
import de.lg.network.Message;
import de.lg.network.NetworkMessageHandler;
import de.lg.network.messages.AuthenticationMessage;
import de.lg.network.messages.PlayerMessage;

/**
 * Created by Lars on 18.01.2016.
 */
public class AuthenticationMessageHandler implements MessageHandler {

    private NetworkMessageHandler networkMessageHandler = null;

    public AuthenticationMessageHandler(NetworkMessageHandler networkMessageHandler) {
        this.networkMessageHandler = networkMessageHandler;
    }

    @Override
    public void handleMessage(Connection connection, Message message) {

        // TODO: authenticate
        AuthenticationMessage authenticationMessage = (AuthenticationMessage) message;
        authenticationMessage.setAuthenticationToken(connection.getAuthenticationToken());

        this.sendAuthenticationInformation(connection, authenticationMessage);
        System.out.println("Client AUTH ID " +authenticationMessage.getAuthenticationToken());

        PlayerMessage playerMessage = new PlayerMessage();
        playerMessage.setIdentifier(connection.getIdentifier());
        playerMessage.setAuthenticationToken(connection.getAuthenticationToken());
        playerMessage.setPlayerIdentifier(connection.getIdentifier());
        this.networkMessageHandler.networkListener.addMessageToSimulation(playerMessage);

    }

    @Override
    public void announce() {

    }

    @Override
    public Class getHandledClass() {
        return AuthenticationMessage.class;
    }

    /**
     * sends greeting to the client
     *
     * @param connection
     * @param authenticationMessage
     */
    private void sendAuthenticationInformation(Connection connection, AuthenticationMessage authenticationMessage) {
        authenticationMessage.setMessageInfo("message contains your identifier, use it for future communication");
        this.networkMessageHandler.getNetworkListener().writeObject(connection, authenticationMessage);
    }

}
