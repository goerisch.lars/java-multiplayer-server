package de.lg.network.handler;

import de.lg.network.Connection;
import de.lg.network.Message;
import de.lg.network.NetworkMessageHandler;
import de.lg.network.messages.KiMessage;

/**
 * Created by Lars on 23.01.2016.
 */
public class KiMessageHandler implements MessageHandler {

    private NetworkMessageHandler networkMessageHandler = null;

    public KiMessageHandler(NetworkMessageHandler networkMessageHandler) {
        this.networkMessageHandler = networkMessageHandler;
    }

    @Override
    public void handleMessage(Connection clientConnection, Message message) {
            this.networkMessageHandler.getNetworkListener().addMessageToSimulation(message);
    }

    @Override
    public void announce() {

    }

    @Override
    public Class getHandledClass() {
        return KiMessage.class;
    }
}
