package de.lg.network.handler;

import de.lg.network.Connection;
import de.lg.network.Message;
import de.lg.network.NetworkMessageHandler;
import de.lg.network.messages.PlayerMessage;

/**
 * Created by Lars on 19.01.2016.
 */
public class PlayerMessageHandler implements MessageHandler {


    private NetworkMessageHandler networkMessageHandler = null;

    public PlayerMessageHandler(NetworkMessageHandler networkMessageHandler) {
        this.networkMessageHandler = networkMessageHandler;
    }

    @Override
    public void handleMessage(Connection clientConnection, Message message) {
        this.networkMessageHandler.getNetworkListener().writeObject(clientConnection,message);
    }

    @Override
    public void announce() {

    }

    @Override
    public Class getHandledClass() {
        return PlayerMessage.class;
    }
}
