package de.lg.network.handler;

import de.lg.network.*;

/**
 * Created by Lars on 18.01.2016.
 */
public interface MessageHandler {

    public void handleMessage(Connection clientConnection, Message message);

    public void announce();

    public Class getHandledClass();

}
