package de.lg.network;

import de.lg.network.handler.*;

import java.util.HashMap;

/**
 * Created by Lars on 02.01.2016.
 */
public class NetworkMessageHandler {

    public NetworkListener networkListener = null;
    public HashMap<String, MessageHandler> messageHandlers = new HashMap<>();

    public NetworkMessageHandler(NetworkListener networkListener) {
        this.networkListener = networkListener;
        initMessageHandlers();
    }

    public void initMessageHandlers() {

        AuthenticationMessageHandler authenticationMessageHandler = new AuthenticationMessageHandler(this);
        registerHandler(authenticationMessageHandler);

        PlayerMessageHandler playerMessageHandler = new PlayerMessageHandler(this);
        registerHandler(playerMessageHandler);

        WorldSectorMessageHandler worldSectorMessageHandler = new WorldSectorMessageHandler(this);
        registerHandler(worldSectorMessageHandler);

        MovementMessageHandler movementMessageHandler = new MovementMessageHandler(this);
        registerHandler(movementMessageHandler);

        KiMessageHandler kiMessageHandler = new KiMessageHandler(this);
        registerHandler(kiMessageHandler);
    }

    /**
     * register an handler
     *
     * @param handler
     */
    private void registerHandler(MessageHandler handler) {
        messageHandlers.put(handler.getClass().toString(), handler);
    }


    public void proceedNetworkMessage(Connection connection, Object object) {

        boolean handled = false;

        for (HashMap.Entry<String, MessageHandler> entry : messageHandlers.entrySet()) {
            Class handledClass = entry.getValue().getHandledClass();

            if (object.getClass().equals(handledClass)) {
                handled = true;
                entry.getValue().handleMessage(connection, (Message) object);
            }
        }

        if(!handled){
            System.err.println("Error: Handler for " + object.getClass() + " not defined");
        }

    }

    public NetworkListener getNetworkListener() {
        return networkListener;
    }
}
