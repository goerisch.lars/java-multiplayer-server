package de.lg.network;

import java.util.ArrayList;

/**
 * Created by Lars on 30.12.2015.
 * <p>
 * global object for network operations
 */
public class NetworkManager {

    private ArrayList<ClientConnection> clientConnectionArrayList = new ArrayList<>();

    public NetworkManager() {

    }

    /**
     * sends an message to an specific clientConnections
     *
     * @param networkMessage - NetworkMessage
     * @param clientConnection - ClientConnection
     */
    public void sendMessage(NetworkMessage networkMessage, ClientConnection clientConnection) {


    }

    /**
     * sends messages to multiple clientConnections
     *
     * @param networkMessage - NetworkMessage
     * @param clientConnections - ArrayList<ClientConnection>
     */
    public void sendMessage(NetworkMessage networkMessage, ArrayList<ClientConnection> clientConnections) {


    }

    /**
     * sends an message to all playerConnections on the server
     *
     * @param networkMessage - NetworkMessage
     */
    public void broadcast(NetworkMessage networkMessage) {


    }

    public NetworkMessage readMessage(ClientConnection clientConnection) {

        NetworkMessage message = new NetworkMessage();

        return message;
    }

    public static void handleNetworkMessage(NetworkMessage networkMessage){

    }


}
