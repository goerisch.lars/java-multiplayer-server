package de.lg.network;

import de.lg.core.Config;
import de.lg.core.SynchronizedQueue;
import de.lg.network.messages.KiMessage;
import de.lg.network.messages.ObjectSerialization;
import de.lg.network.messages.WorldSectorMessage;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousChannelGroup;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.concurrent.*;

/**
 * Created by Lars on 30.12.2015.
 * <p>
 * listening for new connections from clients or servers and handle them to the correct process
 */
public class NetworkListener implements Runnable {

    NetworkMessageHandler networkMessageHandler = null;

    AsynchronousServerSocketChannel serverSocketChannel = null;

    ArrayList<AsynchronousSocketChannel> clientSocketChannels = new ArrayList<>();

    ArrayList<ClientConnection> clientConnections = new ArrayList<>();

    HashMap<String, Connection> connections = new HashMap<>();


    public final static int READ_MESSAGE_WAIT_TIME = 15;
    public final static int MESSAGE_INPUT_SIZE = 4096 * 5;

    // network -> simulation
    private SynchronizedQueue<Message> networkSimulationQueue;

    // simulation -> network
    private SynchronizedQueue<Message> simulationNetworkQueue;

    private CompletionHandler handler = null;

    public NetworkListener(SynchronizedQueue<Message> networkSimulationQueue, SynchronizedQueue<Message> simulationNetworkQueue) {
        this.networkSimulationQueue = networkSimulationQueue;
        this.simulationNetworkQueue = simulationNetworkQueue;
    }

    @Override
    public void run() {

        // init
        this.networkMessageHandler = new NetworkMessageHandler(this);

        System.out.println("NetworkListener running");
        this.openAsynchronousServerSocketChannel();

        this.handler = new CompletionHandler<AsynchronousSocketChannel, Object>() {
            @Override
            public void completed(final AsynchronousSocketChannel asyncSocketChannel, Object attachment) {
                if (serverSocketChannel.isOpen()) {
                    handleAcceptConnection(asyncSocketChannel);
                    serverSocketChannel.accept(null, handler);

                }
            }

            @Override
            public void failed(Throwable exc, Object attachment) {
                if (serverSocketChannel.isOpen()) {
                    serverSocketChannel.accept(null, this);
                    System.out.println("***********" + exc + " statement=" + attachment);
                }
            }
        };
        this.serverSocketChannel.accept(null, handler);

        while (true) {
            if (!this.simulationNetworkQueue.isEmpty()) {
                Collection<Message> messages = this.simulationNetworkQueue.getAll(true);
                for (Message message : messages) {
                    if(message instanceof WorldSectorMessage && ((WorldSectorMessage) message).getAuthenticationToken().equals("SERVERMESSAGE")){
                        broadCast(message);
                    }else{
                        if(message instanceof KiMessage){
                            broadCast(message);
                        }else {
                            sendMessage(getConnectionByIdentifier(message.getAuthenticationToken()), message);
                        }

                    }

                }

            }

            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    private void broadCast(Message message) {
        for (Connection connection: connections.values()){
            this.writeObject(connection.getAsyncSocketChannel(), message);
        }
    }

    /**
     * send an message and add own auth token to message
     *
     * @param connection
     * @param message
     */
    private void sendMessage(Connection connection, Message message) {
        System.out.print(".");
        this.writeObject(connection.getAsyncSocketChannel(), message);
    }

    private void handleAcceptConnection(AsynchronousSocketChannel asyncSocketChannel) {

        ClientConnection clientConnection = new ClientConnection(asyncSocketChannel);
        clientConnection.setAuthenticationToken(AuthenticationToken.generateToken());
        this.addNewConnection(clientConnection);
        this.clientSocketChannels.add(asyncSocketChannel);
        this.clientConnections.add(clientConnection);
        this.connections.put(clientConnection.getAuthenticationToken(), clientConnection);

        Object object = this.readObject(asyncSocketChannel);

        handleMessageObject(clientConnection, object);

        ByteBuffer buffer = ByteBuffer.allocate(MESSAGE_INPUT_SIZE);
        asyncSocketChannel.read(buffer, null, new CompletionHandler<Integer, Object>() {
            @Override
            public void completed(Integer result, Object attachment) {
                if (result > 0) {
                    Object object = ObjectSerialization.toObject(buffer.array());
                    handleMessageObject(clientConnection, object);
                    buffer.clear();
                    asyncSocketChannel.read(buffer, buffer, this);
                } else {
                    removeConnection(clientConnection);
                    System.out.println("bye bye");
                }
            }

            @Override
            public void failed(Throwable exc, Object attachment) {
                System.out.println("client disconnected A");
               //exc.printStackTrace();
            }
        });


    }

    private void handleMessageObject(ClientConnection clientConnection, Object object) {
        if (object instanceof Message) {
            networkMessageHandler.proceedNetworkMessage(clientConnection, object);
        }
    }

    /**
     * open server socket channel with default threaded channel group
     * and add them to global class @var serverSocketChannel
     *
     * @return AsynchronousServerSocketChannel
     */
    private AsynchronousServerSocketChannel openAsynchronousServerSocketChannel() {
        AsynchronousChannelGroup group = null;
        try {
            group = AsynchronousChannelGroup.withFixedThreadPool(10, Executors.defaultThreadFactory());
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("open server channel");
        if (this.serverSocketChannel == null) {
            try {
                this.serverSocketChannel = AsynchronousServerSocketChannel.open(group);
                InetSocketAddress hostAddress = new InetSocketAddress("localhost", Config.serverPort);
                this.serverSocketChannel.bind(hostAddress);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println("server channel open");
        return this.serverSocketChannel;
    }

    /**
     * open server socket channel with default threaded channel group
     * and add them to global class @var serverSocketChannel
     *
     * @return AsynchronousServerSocketChannel
     */
    private void openAsynchronousClientSocketChannel() {

    }

    /**
     *
     */
    private void closeNetwork() {
        try {
            this.serverSocketChannel.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//    public Object readObject(AsynchronousSocketChannel asyncSocketChannel){
//        ObjectInputStream ois = null;
//        try {
//            ois = new ObjectInputStream(Channels.newInputStream(asyncSocketChannel));
//            return ois.readObject();
//        } catch (IOException | ClassNotFoundException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

    private void writeObject(AsynchronousSocketChannel asyncSocketChannel, Object object) {
        try {
            ByteBuffer messageByteBuffer = ByteBuffer.wrap(ObjectSerialization.toByteArray(object));
            Future<Integer> futureWriteResult = asyncSocketChannel.write(messageByteBuffer);
            futureWriteResult.get(READ_MESSAGE_WAIT_TIME, TimeUnit.SECONDS);
            if (messageByteBuffer.hasRemaining()) {
                messageByteBuffer.compact();
            } else {
                messageByteBuffer.clear();
            }
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            try {
                asyncSocketChannel.shutdownOutput();
                asyncSocketChannel.shutdownInput();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

        }
    }

    private Object readObject(AsynchronousSocketChannel asyncSocketChannel) {
        System.out.println("send message to client");
        try {
            ByteBuffer messageByteBuffer = ByteBuffer.allocate(MESSAGE_INPUT_SIZE);
            Future<Integer> futureReadResult = asyncSocketChannel.read(messageByteBuffer);
            futureReadResult.get(READ_MESSAGE_WAIT_TIME, TimeUnit.SECONDS);
            Object object = ObjectSerialization.toObject(messageByteBuffer.array());
            return object;
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            e.printStackTrace();
        }
        return null;
    }


    private ClientConnection getClientConnections() {
        return null;
    }

    public void writeObject(Connection connection, Object object) {
        try {
            ByteBuffer messageByteBuffer = ByteBuffer.wrap(ObjectSerialization.toByteArray(object));
            Future<Integer> futureWriteResult = connection.getAsyncSocketChannel().write(messageByteBuffer);
            futureWriteResult.get(READ_MESSAGE_WAIT_TIME, TimeUnit.SECONDS);
            if (messageByteBuffer.hasRemaining()) {
                messageByteBuffer.compact();
            } else {
                messageByteBuffer.clear();
            }
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            e.printStackTrace();
        }
    }

    public void addNewConnection(Connection connection) {

        this.connections.put(connection.getIdentifier(), connection);

    }

    public void removeConnection(Connection connection) {
        this.connections.remove(connection.getIdentifier());

    }

    public Connection getConnectionByIdentifier(String identifier) {
        return this.connections.get(identifier);

    }

    public void addMessageToSimulation(Message message) {
        this.networkSimulationQueue.add(message);
    }

}
