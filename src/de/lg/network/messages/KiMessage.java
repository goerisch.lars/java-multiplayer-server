package de.lg.network.messages;

import de.lg.network.Message;

import java.io.Serializable;

/**
 * Created by Lars on 23.01.2016.
 */
public class KiMessage extends Message implements Serializable {

    public float moveToX = 0.0f;

    public float moveToY = 0.0f;

    private String kiId = null;



    public String getKiId() {
        return kiId;
    }

    public KiMessage setKiId(String kiId) {
        this.kiId = kiId;
        return this;
    }

    public float getMoveToX() {
        return moveToX;
    }

    public KiMessage setMoveToX(float moveToX) {
        this.moveToX = moveToX;
        return this;
    }

    public float getMoveToY() {
        return moveToY;
    }

    public KiMessage setMoveToY(float moveToY) {
        this.moveToY = moveToY;
        return this;
    }

}
