package de.lg.network.messages;

import de.lg.network.Message;
import de.lg.player.*;

/**
 * message to announce new players to the other players
 * <p>
 * Created by Lars on 19.01.2016.
 */
public class PlayerMessage extends Message {

    // globalIdentifier for this specific player
    public String playerIdentifier = null;

    // spawnPosition
    public float positionX = 0.0f;
    public float positionY = 0.0f;

    public PlayerInformation playerInformation = null;

    public String getPlayerIdentifier() {
        return playerIdentifier;
    }

    public PlayerMessage setPlayerIdentifier(String playerIdentifier) {
        this.playerIdentifier = playerIdentifier;
        return this;
    }

    public float getPositionX() {
        return positionX;
    }

    public PlayerMessage setPositionX(float positionX) {
        this.positionX = positionX;
        return this;
    }

    public float getPositionY() {
        return positionY;
    }

    public PlayerMessage setPositionY(float positionY) {
        this.positionY = positionY;
        return this;
    }
}
