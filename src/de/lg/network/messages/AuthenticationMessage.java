package de.lg.network.messages;

        import de.lg.network.Message;

        import java.io.Serializable;

/**
 * Created by Lars on 18.01.2016.
 */
public class AuthenticationMessage  extends Message implements Serializable {

    private String authenticationId = null;

    private String name = null;

    private String messageInfo = null;

    public AuthenticationMessage(String authenticationId) {
        this.authenticationId = authenticationId;
    }




    // #########

    public String getAuthenticationId() {
        return authenticationId;
    }

    public AuthenticationMessage setAuthenticationId(String authenticationId) {
        this.authenticationId = authenticationId;
        return this;
    }

    public String getName() {
        return name;
    }

    public AuthenticationMessage setName(String name) {
        this.name = name;
        return this;
    }

    public String getMessageInfo() {
        return messageInfo;
    }

    public AuthenticationMessage setMessageInfo(String messageInfo) {
        this.messageInfo = messageInfo;
        return this;
    }
}