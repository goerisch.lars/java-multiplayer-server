package de.lg.network.messages;

import de.lg.network.Message;

import java.io.Serializable;

/**
 * Created by Lars on 18.01.2016.
 */
public class WorldSectorMessage extends Message implements Serializable {

    public String data = "";

    public String type = "";


    public String getData() {
        return data;
    }

    public WorldSectorMessage setData(String data) {
        this.data = data;
        return this;
    }
}