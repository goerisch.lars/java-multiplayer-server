package de.lg.network;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by Lars on 20.01.2016.
 */
public abstract class AuthenticationToken implements Serializable {


    private String authenticationToken = null;

    public String getAuthenticationToken() {
        return authenticationToken;
    }

    public AuthenticationToken setAuthenticationToken(String authenticationToken) {
        this.authenticationToken = authenticationToken;
        return this;
    }

    public static String generateToken(){
        return UUID.randomUUID().toString();
    }
}
