package de.lg.network;

import java.nio.channels.AsynchronousSocketChannel;

/**
 * Created by Lars on 18.01.2016.
 */
public class Connection extends GlobalId {

    // specific socketChannel
    private AsynchronousSocketChannel asyncSocketChannel = null;

    public Connection(AsynchronousSocketChannel asyncSocketChannel){
        this.asyncSocketChannel = asyncSocketChannel;
    }

    public AsynchronousSocketChannel getAsyncSocketChannel() {
        return asyncSocketChannel;
    }

    public void setAsyncSocketChannel(AsynchronousSocketChannel asyncSocketChannel) {
        this.asyncSocketChannel = asyncSocketChannel;
    }
}
