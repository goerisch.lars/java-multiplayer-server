package de.lg.network;

import java.util.UUID;

/**
 * Created by Lars on 19.01.2016.
 */
public abstract class GlobalId extends AuthenticationToken {

    private String identifier = UUID.randomUUID().toString();


    public String getIdentifier() {
        return identifier;
    }

    public GlobalId setIdentifier(String identifier) {
        this.identifier = identifier;
        return this;
    }
}
